package com.kalehase.rectcalculator;


import java.text.DecimalFormat;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends ActionBarActivity {
	
	double width = 0.0;
	double height = 0.0;
	double area = 0.0;
	double perimeter = 0.0;
	
	// Handles XML objects
	EditText editHeight;
	EditText editWidth;
    TextView areaSolution;
    TextView perimSolution;
    

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		 // Get the Reference of EditText
        editHeight=(EditText)findViewById(R.id.height_value);
        editWidth=(EditText)findViewById(R.id.width_value);
        areaSolution=(TextView)findViewById(R.id.area_solution);
        perimSolution=(TextView)findViewById(R.id.perim_solution);

        // Attach TextWatcher to EditHeight
        editWidth.addTextChangedListener(new TextWatcher() {
        	
        	@Override
    	    public void onTextChanged(CharSequence s, int start, int before, int count)
        	{
    	        // Read width & height
        		String widthStr = editWidth.getText().toString();
        		if(widthStr.length() > 0)
        		{
        			width = Double.parseDouble(widthStr);
        		}
        		else
        			width = 0.0;
        		String heightStr = editHeight.getText().toString();
        		if(heightStr.length() > 0)
        		{
        			height = Double.parseDouble(heightStr);
        		}
        		else
        			height = 0.0;
        		
        		// Calculate Area
        		area = calcArea();
        		String areaStr = String.format("%.2f", area);
        		// Calculate Perimeter
        		perimeter = calcPerim();
        		String perimStr = String.format("%.2f", perimeter);
        		// Set Labels
        		areaSolution.setText(areaStr);
        		perimSolution.setText(perimStr);
    	    }
            
        	@Override
    	    public void beforeTextChanged(CharSequence s, int start, int count, int after)
    	    {
    	        // When No Values Entered
    	       areaSolution.setText("0.00");
    	       perimSolution.setText("0.00");
    	    }
    	
        	@Override
    	    public void afterTextChanged(Editable s)
    	    {
        		
    	    }
    	    }

    	);
        
       editHeight.addTextChangedListener(new TextWatcher() {
            
        	@Override
    	    public void beforeTextChanged(CharSequence s, int start, int count, int after)
    	    {
    	        // When No Values Entered
    	       areaSolution.setText("0.00");
    	       perimSolution.setText("0.00");
    	    }
        	
        	@Override
    	    public void onTextChanged(CharSequence s, int start, int before, int count)
    	    {
    	        // Read width & height
        		String widthStr = editWidth.getText().toString();
        		if(widthStr.length() > 0)
        		{
        			width = Double.parseDouble(widthStr);
        		}
        		else
        			width = 0.0;
        		String heightStr = editHeight.getText().toString();
        		if(heightStr.length() > 0)
        		{
        			height = Double.parseDouble(heightStr);
        		}
        		else
        			height = 0.0;
        		// Calculate Area
        		area = calcArea();
        		String areaStr = String.format("%.2f", area);
        		// Calculate Perimeter
        		perimeter = calcPerim();
        		String perimStr = String.format("%.2f", perimeter);
        		// Set Labels
        		areaSolution.setText(areaStr);
        		perimSolution.setText(perimStr);
    	    }
    	
        	@Override
    	    public void afterTextChanged(Editable s)
    	    {
        		
    	    }
    	    }

    	);
	}
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	public double calcArea()
	{
		return width * height;
	}
	
	public double calcPerim()
	{
		return width * 2 + height * 2;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_main, container,
					false);
			return rootView;
		}
	}

}